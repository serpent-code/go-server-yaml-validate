package ServerYamlValidate

import (
	"fmt"
	"os"

	"gopkg.in/yaml.v3"
)

const hostDefaultValue = "localhost"
const ipRequestsPerSecDefaultValue int = 100
const otpSmsInternalSecDefaultValue int = 100

func Int(v int) *int { return &v }

// pointers are used for optional fields
type ServerYaml struct {
	Server      *Server
	Rate_limits *Rate_limits
}

type Server struct {
	Host string `yaml:"host"`
	Port *int   `yaml:"port"`
}

type Rate_limits struct {
	Ip_requests_per_sec  *int `yaml:"ip_requests_per_sec"`
	Otp_sms_interval_sec *int `yaml:"otp_sms_interval_sec"`
}

// To dereference and return actual values to the user
// lower-casing all fields here too, so it becomes like the yaml
type ServerYamlReturn struct {
	server struct {
		host string
		port int
	}
	rate_limits struct {
		ip_requests_per_sec  int
		otp_sms_interval_sec int
	}
}

// ReadAndValidateYamlFile takes the string path to the yaml file,
// validates it and returns a ServerYamlReturn struct and error value
func ReadAndValidateYamlFile(yamlPath string) (ServerYamlReturn, error) {
	var yamlStruct ServerYaml
	var result ServerYamlReturn

	yamlFile, err := os.Open(yamlPath)
	if err != nil {
		return result, fmt.Errorf("error reading the input yaml: %w", err)
	}
	defer yamlFile.Close()

	err = yaml.NewDecoder(yamlFile).Decode(&yamlStruct)
	if err != nil {
		return result, fmt.Errorf("error parsing the input yaml: %w", err)
	}

	if yamlStruct.Server == nil {
		return result, fmt.Errorf("top level 'Server' block not found in input yaml")
	}

	if len(yamlStruct.Server.Host) == 0 {
		yamlStruct.Server.Host = hostDefaultValue
	}

	if yamlStruct.Server.Port == nil {
		return result, fmt.Errorf("port not given in Server block")
	}

	if *yamlStruct.Server.Port < 1 || *yamlStruct.Server.Port > 65535 {
		return result, fmt.Errorf("port not in valid port range")
	}

	if yamlStruct.Rate_limits == nil {
		yamlStruct.Rate_limits = &Rate_limits{
			Ip_requests_per_sec:  Int(ipRequestsPerSecDefaultValue),
			Otp_sms_interval_sec: Int(otpSmsInternalSecDefaultValue),
		}
	} else {
		if yamlStruct.Rate_limits.Ip_requests_per_sec == nil {
			return result, fmt.Errorf("rate_limits block exists but ip_request_per_sec doesn't exist")
		}
		if *yamlStruct.Rate_limits.Ip_requests_per_sec <= 60 || *yamlStruct.Rate_limits.Ip_requests_per_sec >= 1000 {
			return result, fmt.Errorf("ip_requests_per_sec not in valid range")
		}

		if yamlStruct.Rate_limits.Otp_sms_interval_sec == nil {
			yamlStruct.Rate_limits.Otp_sms_interval_sec = Int(100)
		} else {
			if *yamlStruct.Rate_limits.Otp_sms_interval_sec < 60 || *yamlStruct.Rate_limits.Otp_sms_interval_sec >= 300 {
				return result, fmt.Errorf("otp_sms_interval_sec not in valid range")
			}
		}
	}

	result.server.host = yamlStruct.Server.Host
	result.server.port = *yamlStruct.Server.Port
	result.rate_limits.ip_requests_per_sec = *yamlStruct.Rate_limits.Ip_requests_per_sec
	result.rate_limits.otp_sms_interval_sec = *yamlStruct.Rate_limits.Otp_sms_interval_sec

	return result, nil

}
