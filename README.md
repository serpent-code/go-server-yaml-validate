# Go Server Yaml Validate
This is a small Go library reading a yaml file of a certain format (see example-input.yml), validates it according to some rules and returns a struct to be used in another Go program.



## Example Usage
```bash
go mod init test
go get gitlab.com/serpent-code/go-server-yaml-validate
```

```go
package main

import (
"fmt"
"gitlab.com/serpent-code/go-server-yaml-validate"
)

func main() {
	myServerYaml, err := ServerYamlValidate.ReadAndValidateYamlFile("input.yml")

	if err != nil {
		panic(err)
	}

	fmt.Printf("%+v\n", myServerYaml)
}
```

